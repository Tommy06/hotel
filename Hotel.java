import java.util.ArrayList;
import java.util.List;

public class Hotel {
    private List<Room> rooms;

    public Hotel() {
        rooms = new ArrayList<>();
        // Inizializza le stanze dell'hotel
        for (int i = 1; i <= 10; i++) {
            rooms.add(new Room(i));
        }
    }

    public void displayAvailableRooms() {
        System.out.println("Stanze disponibili:");
        for (Room room : rooms) {
            if (room.isAvailable()) {
                System.out.println("Stanza " + room.getRoomNumber());
            }
        }
    }

    public void displayBookedRooms() {
        System.out.println("Stanze prenotate:");
        for (Room room : rooms) {
            if (!room.isAvailable()) {
                System.out.println("Stanza " + room.getRoomNumber() + " - Ospite: " + room.getGuestName() + " - Importo Pagato: $" + room.getAmountPaid());
            }
        }
    }
}
